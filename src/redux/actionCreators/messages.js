import { domain, jsonHeaders, handleJsonResponse } from "./constants";
import {
  GETMESSAGES,
  GETUSERMESSAGES,
  CREATEMESSAGE,
  DELETEMESSAGES
} from "../actionTypes";
const url = domain + "/messages";

export const getMessages = () => dispatch => {
  dispatch({
    type: GETMESSAGES.START
  });

  return fetch(url, {
    method: "GET",
    headers: jsonHeaders
  })
    .then(handleJsonResponse)
    .then(result => {
      return dispatch({
        type: GETMESSAGES.SUCCESS,
        payload: result
      });
    })
    .catch(err => {
      return Promise.reject(dispatch({ type: GETMESSAGES.FAIL, payload: err }));
    });
};

export const getUserMessages = username => dispatch => {
  dispatch({
    type: GETUSERMESSAGES.START
  });
  const endpointUrl = username ? url + "?username=" + username : url;

  return fetch(endpointUrl, {
    method: "GET",
    headers: jsonHeaders
  })
    .then(handleJsonResponse)
    .then(result => {
      return dispatch({
        type: GETUSERMESSAGES.SUCCESS,
        payload: result
      });
    })
    .catch(err => {
      return Promise.reject(
        dispatch({ type: GETUSERMESSAGES.FAIL, payload: err })
      );
    });
};

const _createMessage = messageText => (dispatch, getState) => {
  dispatch({
    type: CREATEMESSAGE.START
  });

  const token = getState().auth.login.result.token;

  return fetch(url, {
    method: "POST",
    headers: { Authorization: "Bearer " + token, ...jsonHeaders },
    body: JSON.stringify({ text: messageText })
  })
    .then(handleJsonResponse)
    .then(result => {
      return dispatch({
        type: CREATEMESSAGE.SUCCESS,
        payload: result
      });
    })
    .catch(err => {
      return Promise.reject(
        dispatch({ type: CREATEMESSAGE.FAIL, payload: err })
      );
    });
};

//chained action creators
export const createMessage = messageText => (dispatch, getState) => {
  return dispatch(_createMessage(messageText)).then(() =>
    dispatch(getMessages())
  );
};
const _deleteMessages = (messageId, username) => (dispatch, getState) => {
  dispatch({ type: DELETEMESSAGES.START });
  const token = getState().auth.login.result.token;
  return fetch(url + "/" + messageId, {
    method: "DELETE",
    headers: { Authorization: "Bearer " + token, ...jsonHeaders }
  })
    .then(handleJsonResponse)
    .then(result => {
      return dispatch({
        type: DELETEMESSAGES.SUCCESS,
        payload: result
      });
    })
    .catch(err => {
      return Promise.reject(
        dispatch({ type: DELETEMESSAGES.FAIL, payload: err })
      );
    });
};
//chained action creators
//deleteMessages -> getMessages
export const deleteMessages = (messageId, username) => (dispatch, getState) => {
  dispatch(_deleteMessages(messageId));
  dispatch(getMessages());
  dispatch(getUserMessages(username));
};
