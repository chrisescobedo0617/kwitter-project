import React, { Component } from "react";
import {
  NavMenu,
  MessageList,
  CreateMessageForm,
  SearchBar,
  Grid
} from "../components";
import { userIsAuthenticated } from "../HOCs";

class MessageFeed extends Component {
  render() {
    return (
      <>
        <Grid columns="three" divided>
          <Grid.Column width={2} 
          style={{ padding: "0" }}>
            <NavMenu isAuthenticated={this.props.isAuthenticated} />
          </Grid.Column>

          <Grid.Column style={{ backgroundColor: "#c5c1c0" }} width={4}>
            <h3 style={{ margin: "30px 0 10px 0", textAlign: "center" }}>
              Messages
            </h3>
            <SearchBar />
            <MessageList />
          </Grid.Column>

          <Grid.Column width={10}>
            <CreateMessageForm />
          </Grid.Column>
        </Grid>
      </>
    );
  }
}

export default userIsAuthenticated(MessageFeed);
