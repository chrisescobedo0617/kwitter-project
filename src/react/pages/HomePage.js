import React, { Component } from "react";
import {
  HomePageFeed,
  Grid,
  TrendingNewsFeed,
  NavMenu,
  SearchBar,
  MessageList
} from "../components";
import { userIsAuthenticated } from "../HOCs";

class HomePage extends Component {
  render() {
    return (
      <>
        <Grid columns="three" divided>
          <Grid.Column 
          style={{
            padding: "0"
          }}
          width={2}>
            <NavMenu isAuthenticated={this.props.isAuthenticated} />
          </Grid.Column>

          <Grid.Column 
          style={{
            backgroundColor: "#C5C1C0"
          }}
          width={10}>
            <HomePageFeed />
            <MessageList 
            style={{ borderBottom: "1px solid #1a2930" }} />
          </Grid.Column>

          <Grid.Column 
          style={{
            backgroundColor: "#1A2930"
          }}
          width={4}>
            <SearchBar />
            <TrendingNewsFeed />
          </Grid.Column>
        </Grid>
      </>
    );
  }
}

export default userIsAuthenticated(HomePage);
