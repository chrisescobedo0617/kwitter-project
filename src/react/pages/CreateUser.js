import React, { Component } from "react";
import { CreateUserProfileForm, Card } from "../components";
import { userIsNotAuthenticated } from "../HOCs";
import "./CreateUser.css";
class CreateUser extends Component {
  render() {
    return (
      <React.Fragment>
        <div id="flex">
          <Card id="createUser">
          <CreateUserProfileForm />
        </Card>
        <div id="flexBackground">
        </div>
        </div>
      </React.Fragment>
    );
  }
}

export default userIsNotAuthenticated(CreateUser);
