import React, { Component } from "react";
import { LoginForm, Card } from "../components";
import { userIsNotAuthenticated } from "../HOCs";
import "./Home.css";

class Home extends Component {
  render() {
    return (
      <>
        <div id="flex">
          <Card id="Page">
            <LoginForm />
          </Card>
          <div id="flexBackground"></div>
        </div>
      </>
    );
  }
}

export default userIsNotAuthenticated(Home);
