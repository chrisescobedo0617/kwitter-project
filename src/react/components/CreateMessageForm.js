import React, { Component } from "react";
import { Button, Container, Spinner } from "../components";
import { withAsyncAction } from "../HOCs";

import "./CreateMessageForm.css";

class CreateMessageForm extends Component {
  state = {
    messageText: ""
  };

  handleClick = event => {
    event.preventDefault();
    this.props.createMessage(this.state.messageText);
    this.setState({ messageText: "" });
  };
  
  handleChangeMessage = event => {
    this.setState({ messageText: event.target.value });
  };
  render() {
    // const { log } = this.state;
  const { loading, error } = this.props;
    return (
      <>
        {/*Might need to be wrapped in a form tag so we can press enter to submit as well as click */}
        
          <h3 style={{ margin: "30px 0 55px 0", textAlign: "center" }}>
            Thread
            <Button.Group
              style={{ border: "none", padding: "0 20px" , color:"white"}}
              basic
              
              floated="right"
            >
            </Button.Group>
          </h3>
{/* 
          <Comment.Group style={{ width: "100%", padding: "20px" }}> */}
           <Container style={{ width: "90%", height: "200px",  border: "1px solid #c5c1c0", borderRadius: "5px", boxShadow: "10px 10px 10px grey" }}>
            <div className="compose">
              <input
                type="text"
                className="compose-input"
                placeholder="Type a message..."
                onChange={this.handleChangeMessage}
                value={this.state.messageText}
              />
            </div>
          {/* </Comment.Group> */}

          <Button
            type="submit"
            onClick={this.handleClick}
            disabled={this.props.loading}
            content="Send"
            labelPosition="left"
            icon="send"
            primary
            style={{ position: "relative", top: "35px", left: "30px", backgroundColor:"#95b9c7"}}
          />
          {loading && <Spinner name="circle" color="white" />}
          {error && <p style={{ color: "red" }}>{error.message}</p>}
          
        </Container>
      </>
    );
  }
}
export default withAsyncAction("messages","createMessage" ) (CreateMessageForm)