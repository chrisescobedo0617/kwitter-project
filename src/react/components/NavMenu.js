import React, { Component } from "react";
import { Link } from ".";
import { Sidebar, Segment, Menu, Icon } from ".";
import "./NavMenu.css";
import { withAsyncAction, connect } from "../HOCs";

class NavMenu extends Component {
  handleLogout = event => {
    event.preventDefault();
    this.props.logout();
  };

  render() {
    return (
      <div id="menu">
        {this.props.isAuthenticated && (
          <div id="NavMenu-links">
            <Sidebar.Pushable
              as={Segment}
              id="nav_segment"
              style={{
                margin: "0"
              }}
            >
              <Sidebar
                style={{
                  width: "100%",
                  backgroundColor: "#1A2930",
                  position: "fixed",
                  top: "0px"
                }}
                as={Menu}
                icon="labeled"
                vertical
                visible
                width="wide"
              >
                <div id="menu">
                  <h1 
                  style={{
                    color: "white"
                  }}
                  >Kwitter</h1>
                </div>
                <Link to="/homepage">
                  <Menu.Item
                  style={{
                    color: "white"
                  }}
                  >
                    <Icon name="home" />
                    Home
                  </Menu.Item>
                </Link>
                <Link to={"/profile/" + this.props.username}>
                  {" "}
                  <Menu.Item 
                  style={{
                    color: "white"
                  }}>
                    <Icon name="user" />
                    Profile
                  </Menu.Item>
                </Link>
                <Link to="/messagefeed">
                  {" "}
                  <Menu.Item
                  style={{
                    color: "white"
                  }}
                  >
                    <Icon name="envelope" />
                    Message
                  </Menu.Item>
                </Link>
                <Link to="/" onClick={this.handleLogout}>
                  {" "}
                  <Menu.Item
                  style={{
                    color: "white"
                  }}
                  >
                    <Icon name="sign-out" />
                    Logout
                  </Menu.Item>
                </Link>
              </Sidebar>
            </Sidebar.Pushable>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    username: state.auth.login.result.username
  };
};

export default connect(mapStateToProps)(
  withAsyncAction("auth", "logout")(NavMenu)
);
