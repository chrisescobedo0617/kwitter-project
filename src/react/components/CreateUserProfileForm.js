import React, { Component } from "react";
import { Form, Button, Segment, Link } from "../components";
import { withAsyncAction } from "../HOCs";
import "./CreateUserProfileForm.css";

class CreateUserProfileForm extends Component {
  state = {
    username: "",
    displayName: "",
    password: ""
  };
  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  handleCreateUser = e => {
    this.props.createUser(
      this.state.username,
      this.state.displayName,
      this.state.password
    );
  };

  render() {
    return (
      <React.Fragment>
        <Segment
          style={{
            width: "50%",
            margin: "0 auto",
            backgroundColor: "transparent"
          }}
        >
          <Form
            inverted
            id="login-form"
            style={{ alignItems: "center", width: "100%", margin: "40px auto" }}
            o
          >
            <h2 style={{ "": "40px", marginTop: "75px", color: "white" }}>
              Your Favorite Social Platform!
            </h2>
            <h3 style={{ marginBottom: "40px", color: "white" }}>
              Register Your Account
            </h3>
            <Form.Group widths="equal" style={{ width: "100%" }}>
              <Form.Input
                fluid
                type="text"
                name="username"
                label="Username:"
                placeholder="Username"
                autoFocus
                required
                onChange={this.handleChange}
              />
            </Form.Group>
            <Form.Group widths="equal" style={{ width: "100%" }}>
              <Form.Input
                fluid
                type="text"
                name="displayName"
                label="Display Name:"
                placeholder="Display Name"
                autoFocus
                required
                onChange={this.handleChange}
              />
            </Form.Group>
            <Form.Group widths="equal" style={{ width: "100%" }}>
              <Form.Input
                fluid
                type="password"
                label="Password:"
                name="password"
                placeholder="Password"
                required
                onChange={this.handleChange}
              />
            </Form.Group>
            <Form.Group widths="equal" style={{ width: "100%" }}>
              <Form.Input
                fluid
                type="password"
                label="Confirm Password:"
                name="password"
                placeholder="Confirm Password"
                required
                onChange={this.handleChange}
              />
            </Form.Group>

            <Button
              type="submit"
              onClick={this.handleCreateUser}
              style={{ marginTop: "10px", width: "95%", color: "white", backgroundColor: "#2c85d0"}}
            >
              Submit
            </Button>
            <h5 style={{ "": "30px", alignSelf: "center"}}>
              Have an account?{" "}
              <Link to="/" id="loginLink">
                 Login
              </Link>{" "}
            </h5>
          </Form>
        </Segment>
      </React.Fragment>
    );
  }
}
export default withAsyncAction("users", "createUser")(CreateUserProfileForm);
