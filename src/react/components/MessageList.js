import React, { Component } from "react";
import {
  Card,
  Image,
  DeleteMessageButton,
  Container,
  Button
} from "../components";
import "./MessageList.css";
import { withAsyncAction, connect } from "../HOCs";

import { getUsers } from "../../redux/actionCreators";

class MessageList extends Component {
  componentDidMount() {
    this.props.getMessages(this.props.username);
    this.props.getUsers(this.props.users);
  }
  componentDidUpdate(prevProps) {
    if (this.props.username !== prevProps.username) {
      this.props.getUserMessages(this.props.username);
    } else {
      this.props.getMessages();
    }
  }
  render() {
    console.log(this.props.loggedInUser);
    return (
      this.props.result &&
      this.props.result.messages.map(message => {
        return (
          <Card.Group key={message.id}>
            <Card.Content className="conversation-list-item">
              <Image
                className="conversation-photo"
                src="https://react.semantic-ui.com/images/avatar/large/steve.jpg"
                // src={
                //  this.props.user.user.pictureLocation
                //     ? "https://kwitter-api.herokuapp.com" + this.props.user.user.pictureLocation
                //     : "http://simpleicon.com/wp-content/uploads/user1.svg"
                // }

                // src={
                //  this.props.user.user.pictureLocation
                //     ? "https://kwitter-api.herokuapp.com" + this.props.user.user.pictureLocation
                //     : "http://simpleicon.com/wp-content/uploads/user1.svg"
                // }
              />
              <Container className="conversation-info">
                <Card.Header className="conversation-title">
                  {message.username}
                </Card.Header>
                <Card.Description className="conversation-snippet">
                  {message.text}
                </Card.Description>
                <Button
                  as="div"
                  labelPosition="right"
                  size="mini"
                  compact
                  onClick={this.handleClick}
                  style={{ marginTop: "15px" }}
                >
                  {/* <Button
                    size="mini"
                    style={{ padding: "8px 14px", backgroundColor: "#95b9c7" }}
                  >
                    <Icon name="heart" />
                  </Button> */}
                  {/* <Label as="a" basic pointing="left" size="mini"> */}
                  {/* {this.props.likes.length} */}
                  {this.props.addLikes}
                  {/* </Label> */}
                </Button>
              </Container>
              {this.props.loggedInUser === message.username ? (
                <div>
                  <DeleteMessageButton
                    id={message.id}
                    username={message.username}
                  />
                </div>
              ) : (
                <div></div>
              )}
              {/* <DeleteMessageButton
                id={message.id}
                username={message.username}
              /> */}
            </Card.Content>
          </Card.Group>
        );
      })
    );
  }
}
//trying
const mapStateToProps = state => {
  return {
    user: state.users.getUser.result,
    loggedInUser: state.auth.login.result.username,
    getUsers
  };
};
export default connect(mapStateToProps)(
  withAsyncAction("messages", "getMessages")(MessageList)
);
