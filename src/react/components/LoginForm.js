import React, { Component } from "react";
import { Spinner, Segment, Form, Button, Link } from ".";
import { withAsyncAction } from "../HOCs";
import "./LoginForm.css";

class LoginForm extends Component {
  state = { username: "", password: "" };

  handleLogin = e => {
    e.preventDefault();
    this.props.login(this.state);
  };

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  render() {
    const { loading, error } = this.props;
    return (
      <React.Fragment>
        <Segment
          inverted
          id="login"
          style={{
            width: "50%",
            margin: "25% auto",
            backgroundColor: "transparent"
          }}
        >
          <Form
            inverted
            id="login-form"
            style={{ alignItems: "center", width: "100%", margin: "40px auto" }}
            onSubmit={this.handleLogin}
          >
            <h2 style={{ "": "40px", marginBottom: "30px", opacity: "1" }}>
              Your Favorite Social Platform!
            </h2>
            <Form.Group widths="equal" style={{ width: "100%" }}>
              <Form.Input
                fluid
                type="text"
                name="username"
                label="Username:"
                placeholder="Username"
                autoFocus
                required
                onChange={this.handleChange}
              />
            </Form.Group>
            <Form.Group widths="equal" style={{ width: "100%" }}>
              <Form.Input
                fluid
                type="password"
                label="Password:"
                name="password"
                placeholder="Password"
                required
                onChange={this.handleChange}
              />
            </Form.Group>

            <Button type="submit" primary disabled={loading} style={{ width: "95%", marginTop: "20px", color: "white", backgroundColor: "#2c85d0" }}>
              {" "}
              Login{" "}
            </Button>
            <h5 style={{ "": "30px" }}>
              Don't have an account? <Link to="/createuser" id="signupLink">Join Kwitter</Link>{" "}
              today.
            </h5>
          </Form>
          {loading && <Spinner name="circle" color="white" />}
          {error && <p style={{ color: "red" }}>{error.message}</p>}
        </Segment>
      </React.Fragment>
    );
  }
}

export default withAsyncAction("auth", "login")(LoginForm);
